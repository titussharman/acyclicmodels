\documentclass{amsart}
\usepackage{amscd}
\theoremstyle{definition}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\theoremstyle{remark}
\newtheorem*{remark}{Remark}
\newtheorem*{answer}{Answer}
\theoremstyle{theorem}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\usepackage{enumitem}
\DeclareMathOperator{\op}{op}
\DeclareMathOperator{\Tor}{Tor}
\DeclareMathOperator{\id}{id}
\newcommand{\zz}{\mathbb{Z}}
\newcommand{\rp}{\mathbb{R}P^2}
\newcommand{\rpe}{\mathbb{R}P^3}
\usepackage{calrsfs}
\usepackage{amsrefs}
\renewcommand{\q}{q}
\usepackage{tikz-cd}
\raggedright
\usepackage{hyperref}
\begin{document}

\title{Computing Some Products}

\author{Titus Sharman}
\maketitle

\section{Introduction}

We wish to compute some products, since last week I wasn't sure how to compute these things.   Notice that we are working with $\zz$-modules, ie abelian groups, throughout.   Moreover, since there are isomorphisms $A \otimes B \simeq B \otimes A$ and $\Tor(A, B) \simeq \Tor(B, A)$ (Proposition 3.1 and Exercise 2, chapter 3 of Vick), it suffices to show one direction.

\section{Tensor Products}

How should we compute tensor products?  Recall that we had the following really nice universal property definition:

\begin{definition}
  The \textit{tensor product} of $R$-modules $M$ and $N$ is the universal object for bilinear maps out of $M \times N$.  That is to say, the tensor product consists of another $R$-module $M \otimes N$, together with a map $\phi: M \times N \to M \otimes N$ such that for every bilinear map\footnote{Recall that this might not necessarily be (and usually isn't) a morphism of $R$-modules} $f: M \times N \to X$ for any $X$, $f$ factors uni\q{}uely through $M \otimes N$.  This is summarized in the following commutative diagram:

  \[
    \begin{tikzcd}
      M \times N \arrow[rd, "f"] \arrow[r, "\phi"] & M \otimes N \arrow[d, "\exists ! f_*"]\\
      & X\\
    \end{tikzcd}
  \]

Since this is a universal property construction, the tensor product is uni\q{}ue up to uni\q{}ue isomorphism.  

We didn't, however, know whether the tensor product actually existed.  So in Vick we did this fancy construction of taking the free group on $M \times N$ as a set, \q{}uotienting out by some e\q{}uivalence relations, defining a map $(m, n) \mapsto \{m \otimes n\}$, and then showing that our construction was a tensor product.  Not every element in $M \otimes N$ is of the form $m \otimes n$, but every element is a sum of these primitive tensors.

\begin{remark}

It actually is helpful to know what this construction is.  It's $F(M \times N)$ mod distributivity over the tensor.   More explicitly, we re\q{}uire $m \otimes (n + n') =m \otimes n + m \otimes n'$.  
\end{remark}

A helpful trick for computing tensor products is using the relations $n(a \otimes b) = na \otimes b = a \otimes nb$ and $a \otimes (b + c) = a \otimes b + a \otimes c$.  This is illustrated very nicely by the warning in Chapter XVI.1 of Lang and is our first example below.  After that, we will show what we used extensively last meeting: that $\zz$ (a free $\zz$-module) tensor anything is 0. Then, we will solve the problem I had last week with computing $\zz/2\zz \otimes \zz/2\zz$
  
\end{definition}

\subsection{$\zz/m\zz \otimes \zz/n\zz$ for $m,n$ relatively prime}

Take $a \in \zz/m\zz, b \in \zz/n\zz$.  Then, $na \otimes b = 0 \otimes b = 0, ma \otimes b = a \otimes mb = a \otimes 0 = 0$  Since $m, n$ are relatively prime, $a \otimes b$ is some linear combination of $ma \otimes b$ and $na \otimes b$ so therefore $a \otimes b = 0$\footnote{See the wikipedia article on Bezout's identity}

    Thus any element $\sum a_i \otimes b_i$ in $\zz/m\zz \otimes \zz/n\zz$ is e\q{}ual to 0.  So $\zz/m\zz \otimes \zz/n\zz = 0$


%\subsection{$\zz \otimes \mathrm{anything}$}

    \subsection{$\zz/2\zz \otimes \zz/2\zz$}

    We can just use Proposition 3.4 in Vick.  We have

$$
  \zz/2\zz \otimes \zz/2\zz = 
  \frac{\zz \otimes \zz}{\mathrm{im}(i_1\otimes \mathrm{id}) + \mathrm{im}(\mathrm{id} \otimes i_2)} = \frac{\zz}{2 \zz + 2\zz} = \zz/2\zz
$$

\section{Tor Products}

Suppose we have $R$-modules $M, N$ and a free resolution

\[
\ldots \to F_2 \to F_1 \to F_0 \to M \to 0
\]

of $M$.  Unfortunately, tensoring with $N$ is not \emph{left-exact}\footnote{But it is right-exact, see Proposition XVI.2.6 in Lang}, meaning that if we ``tensor the se\q{}uence with $N$'' (ie consider $\ldots \to F_1 \otimes N \to F_0 \otimes N \to M \otimes N \to 0$ with the maps $\tau$ between elements of the original se\q{}uence turning into $\tau \otimes \id$) the resulting se\q{}uence will not in general be exact. Intuitively, we want to measure how far this new se\q{}uence fails to be exact.

Notice that in the case of $\zz$-modules, we will always be able to find a free resolution that is  an SES.\footnote{This construction is not done explicitly in Barr, although they refer to a proof in Spanier.  The general idea is to take $F_0$ to be the free group on $M$ and then everything else is forced by the need for this to be an SES.  Showing that $F_1$ is free is nontrivial, however}  Because of this, in Barr they \emph{define}  a free resolution of $M$ to be $0 \to F_1 \to F_0 \to M$.  Then, they define Tor to be the kernel of the first map in the following exact se\q{}uence:

$$
F_1 \otimes N \to F_0 \otimes N \to M \otimes N \to 0
$$

More generally, we can construct the Tor functor as follows:  Take our original long exact se\q{}uence.  Tensor the entire thing with $N$ (ie, apply the functor $- \otimes N$) and chop off the last term to get

$$
\ldots F_2 \otimes N \to F_1 \otimes N \to F_0 \otimes N \to 0
$$

Then $\Tor_i^R(M,N)$ is the $i$th homology of this chain complex.  Notice that for $i = 1$ this corresponds with the Barr definition given for $\zz$-modules.  For $i = 0$, we have $\Tor_0(M, N) \simeq M \otimes N$ 

How do we compute Tor, however?  In general it seems hard to find a free resolution of something.  We notice that this is supposed to hold for any free resolution of $M$.  So in practice, we will try to find a free resolution that terminates \q{}uickly.  This is illustrated in the example below.


\subsection{$\Tor(\zz/2\zz, \zz/2\zz)$ (for review)}


Take our free resolution of $\zz/2\zz$ to be

\[
\begin{tikzcd}[column sep=large]
  0  \arrow[r] & \zz  \arrow[r, "\times 2"] &\zz  \arrow[r, "\mod 2"] &\zz/2\zz  \arrow[r] &0
  \end{tikzcd}
\]

Now we tensor with $\zz/2\zz$ to get

$$
\begin{CD}
  \zz \otimes \zz/2\zz @>\times 2 \otimes \id>> \zz \otimes \zz/2\zz @>\mod 2 \otimes \id>> \zz/2\zz \otimes \zz/2\zz \to 0
\end{CD}
$$

But by the computation we did above in section 2.2, we can draw the following commutative diagram

$$
\begin{CD}
  \zz \otimes \zz/2\zz @>\times 2 \otimes \id>> \zz \otimes \zz/2\zz @>\mod 2 \otimes \id>> \zz/2\zz \otimes \zz/2\zz @>>> 0\\
  \phi@|\phi @| @| @.\\
  \zz/2\zz @>\gamma>> \zz/2\zz @>>> \zz/2\zz @>>> 0\\
  \end{CD}
  $$

  where $\phi: \zz \otimes \zz/2\zz \to \zz/2\zz$ is an isomorphism.  We want to find $\ker \gamma$.  But since we know that the map from $\zz \otimes \zz/2\zz \to \zz/2\zz \otimes \zz/2\zz$ is just $n \otimes m \mapsto 1 \otimes mn$, it is clear that the kernel is $\zz/2\zz$


\subsection{$\Tor(\zz, \mathrm{anything})$ }

We want to compute $\Tor(\zz, M)$ for any abelian group $M$.  Suppose we have a finite free resolution

$$
0 \rightarrow F_1 \rightarrow F_0 \rightarrow M \rightarrow 0
$$

Now, when we tensor with $\zz$ we get
$$
0 \to F_1 \otimes \zz \to F_0 \otimes \zz \to M \otimes \zz \to 0
$$


But $\zz$ is a free $\zz$-module, so this new se\q{}uence will still be exact!!\footnote{It turns out that tensoring with any free module is an exact functor.  See \url{https://en.wikipedia.org/wiki/Flat_module}}
%fuck you Titus, do you have any idea why this is the case?  dont try appealing to projective modules either because that's category theory you haven't learned yet
Then  $\Tor(M, \zz) = \ker(F_1 \otimes \zz \to F_0 \otimes \zz) = \mathrm{im}(0 \to F_1 \otimes \zz) = 0$

As before, the induced map $\mathrm{id} \otimes m$ becomes $n \otimes m \mapsto mn$ but this time in $\zz/n\zz$.  So evidently, the kernel will be $\zz/\mathrm{gcd}(m,n)\zz$



\subsection{$\Tor(\zz/n\zz, \zz/m\zz)$}

As before, we take a projective resolution

$$
\begin{CD}
  0 @>>> \zz @>\times m>> \zz @>>> \zz/m\zz @>>> 0\\
\end{CD}
$$

Again we consider

$$
\zz \otimes \zz/n\zz \to \zz/n\zz \otimes \zz \to \zz/n\zz \otimes \zz/m\zz \to 0
$$

As with the 



\begin{bibdiv}
  \begin{biblist}
    \bib{Barr}{book}{
      title={Acyclic Models},
      author={Barr, Michael},
      date={2002},
      publisher={American Mathematical Society}
      }
    \bib{Lang}{book}{
      title={Algebra},
      author={Lang, Serge},
      date={2002},
      publisher={Springer},
      address={New York}
    }
    \bib{Vick}{book}{
      title={Homology Theory: An Introduction to Algebraic Topology},
      author={Vick,James},
      date={1994},
      publisher={Springer},
      address={New York}
    }
    \bib{Weibel}{book}{
      title={An Introduction to Homological Algebra},
      author={Weibel, Charles},
      date={1994},
      publisher={Cambridge University Press}
      }
  \end{biblist}
\end{bibdiv}

\end{document}