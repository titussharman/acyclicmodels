\documentclass{amsart}
\usepackage{amscd}
\theoremstyle{definition}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\theoremstyle{remark}
\newtheorem*{remark}{Remark}
\newtheorem*{answer}{Answer}
\theoremstyle{theorem}
\usepackage{hyperref}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\usepackage{enumitem}
\DeclareMathOperator{\op}{op}
\DeclareMathOperator{\Tor}{Tor}
\newcommand{\zz}{\mathbb{Z}}
\newcommand{\rp}{\mathbb{R}P^2}
\newcommand{\rpe}{\mathbb{R}P^3}
\usepackage{calrsfs}
\usepackage{amsrefs}
\renewcommand{\q}{q}
\raggedright
\begin{document}

\title{Notes on Homological Algebra by Cartan and Eilenberg}

\author{Titus Sharman}
\maketitle

\section{Rings and Modules}

\begin{definition}
  Let $R$ be a unital ring.   A \textit{left module} over $R$ is an abelian group $A$ with a scalar  tmultiplication operation that is associative, etc. 
\end{definition}

\begin{example}
  Notice that a $\zz$ module is just an abelian group, ie. it is isomorphic to its underlying group $A$.  We have remarked on this a lot; it just follows from the fact that we can consider $na, a \in A$ as just $\underbrace{a + a + \ldots}_{n \text{ times}}$
\end{example}

\begin{example}
  If $R$ is a field, then an $R$-module is just a vector space.
\end{example}

Other definitions which we have already seen/already know from Barr:

\begin{itemize}
\item A \textit{homomorphism} $f: A \to B$ between $R$-modules $A, B$ is a homomorphism between $A$ and $B$ as groups, additionally satisfying $f(\lambda x) = \lambda (fx)$ for $\lambda \in R$
\item The \textit{kernel} of $f$ is the submodule of $A$ consisting of all $x \in A$ such that $fx = 0$
\item The \textit{image} of $f$ is $\{fx: x \in A\}$
\item The \textit{coimage} of $f$ is $A/\ker f$
\item The \textit{cokernel} of $f$ is $B/\mathrm{im}(f)$
\item A \textit{monomorphism} $f$ satisfies $\ker f = 0$
  \begin{remark}
    Notice that a monomorphism is not always left invertible, although every left invertible map is a monomorphism.  This is a misconception I had earlier.
  \end{remark}
\item An \textit{epimorphism} $f$ satisfies $\mathrm{coker}(f) = 0$
  \begin{remark}
    Woah this makes the duality between monomorphisms and epimorhisms way more obvious than the standard definition $\mathrm{im}(f) = B$
  \end{remark}
\item An \textit{isomorphism} is an invertible homomorphism (e\q{}uivalently, both a mono and an epi-morphism)
\item A se\q{}uence of homomorphisms is \textit{exact} if the kernel of the preceding homomorphism is the image of the next one
\item An SES
  $$
0 \to A' \to A \to A/A' \to 0
  $$
  is \textit{split exact} if $\mathrm{im}(A' \to A)$ is a direct summand of $A$
\item Let $F$ be an $R$-module, $X \subseteq F$.  $F$ is \textit{free} with \textit{basis} $X$ if every $x \in F$ can be written uni\q{}uely as a finite sum $\sum \lambda_ix_i, \lambda_i \in R, x_i \in X$
\end{itemize}

What if we have $\Lambda$, A

% direct sum and direct product stuff




\begin{bibdiv}
  \begin{biblist}
    \bib{Cartan}{book}{
      title={Homological Algebra},
      author={Cartan, Henri},
      author={Eilenberg, Samuel},
      date={1956},
      publisher={Princeton University Press},
      address={Princeton, New Jersey}
      }
  \end{biblist}
\end{bibdiv}

\end{document}