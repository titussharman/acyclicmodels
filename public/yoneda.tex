\documentclass{amsart}
\usepackage{amscd}
\theoremstyle{definition}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\theoremstyle{remark}
\newtheorem*{remark}{Remark}
\newtheorem*{answer}{Answer}
\theoremstyle{theorem}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\usepackage{enumitem}
\DeclareMathOperator{\op}{op}
\usepackage{calrsfs}
\newcommand{\q}{q}
\raggedright
\begin{document}

\title{The Yoneda Lemma}
\author{Titus Sharman}
\maketitle

\section{Intro and Prere\q{}s}

First we will state the Yoneda Lemma and then we will figure out what it is saying.  The proof follows the commutative diagram from MacLane, \textit{Categories for the Working Mathematician}, although I will stick to the notation and problem statement from Barr.

% \section{Motivation}

% What we really want to do here is to find a nice way of describing an object $B$ in a category $\mathcal{C}$ just by using maps to or from $B$, ie the hom sets from $B$ to other things. This is kind of like a universal property for objects.


\subsection{Hom Functors}


Recall that we already had a pair of functors, the covariant and contravariant hom functors, from $\mathcal{C} \to \mathbf{Set}$ that captured the idea of associating objects with their hom sets, either to or from $B$.  The covariant hom functor takes $X \in \mathrm{Obj}(\mathcal{C})$ to $\hom(B, X)$ and maps $f: X \to Y$ to the set map from $\hom(B, X) \to \hom(B ,Y)$ which takes maps $g: B \to X$ to $f \circ g$.  We can summarize this in the following commutative diagram:

$$
\begin{CD}
  A @>g>> X\\
  @Vf \circ gVV @VVfV\\
  Y @= Y\\
\end{CD}
$$

The contravariant hom functor $\hom(-, B)$ is defined very similarly.

\subsection{Elements of a category}

Recall that elements of an object $X$ in a category are just maps from the terminal object $T$ (if it exists) to $X$.  Notice that in a concrete category (something that looks like \textbf{Set} with additional structure), this corresponds with our intuitive notion of element because the terminal object (up to uni\q{}ue isomorphism) is just a single point and then a map from a 1 element set to another set picks an element from that other set.

\subsection{Thoughts about Natural Transformations}

Recall definition 15, from the Barr notes: If $F, G: \mathcal{C} \to \mathcal{D}$ are functors, then a natural transformation $\lambda: F \to G$ associates each object $C \in \mathrm{Obj}(\mathcal{C})$ with arrow $\lambda C$ such that

$$
\begin{CD}
  F(C) @>\lambda C>> G(C)\\
  @VF(g)VV @VVG(g)V\\
  F(C') @>>\lambda C'> G(C')\\
\end{CD}
$$

commutes.
\section{Statement of the Yoneda Lemma}

Let's just start out by stating the Yoneda Lemma as given in Barr.

\begin{lemma}
  The map $\phi: \mathbf{Nat}(\hom(B, -), F) \to F(B)$ defined by $\phi(\lambda) = \lambda B(1_B)$ is a natural isomorphism of the functors defined in the preceding paragraph.
\end{lemma}

I think they overcomplicate things in Barr here.  By the functors in the preceding paragraph, they mean functors $\mathcal{C} \times \mathrm{Func}(\mathcal{C}, \mathbf{Set}) \to \mathbf{Set}$ defined by $(B, F) \mapsto \mathbf{Nat}(\hom(B,-), F)$ and $(B,F) \mapsto F(B)$.  In other material such as MacLane they just talk about an isomorphism (which is a bijection in \textbf{Set}) between the set $F(B)$ and the set of natural transformations from $\hom(B, -)$ to $F$.  The reason Barr states it in this way is so that we can say that the Yoneda map is ``natural'' (back to our previous discussion of canonical/natural maps)

\bigskip

So what's the hard part of the bijection?  In the statement of the Yoneda lemma we have straight up shown the association between elements of $\mathbf{Nat}(\hom(B, -), F)$ and $F(B)$: this is defined by $\phi(\lambda) = \lambda B(1_B)$. Why does this work?  The $B$ component of $\lambda$ is just a map $\lambda B: \hom(B, B) \to F(B)$.  Therefore it clearly associaties $1_B \in \hom(B,B)$ with an element of $F(B)$.  So the hard part of the bijection is showing that \emph{all} natural transformations from $\hom(B, -)$ to $F$ are of this form.


\bigskip

For the inverse map, we want to take in an element $u$ in the set $F(B)$ and associate an entire natural transformation $\lambda$ to that element, such that $\lambda B(1_B) = u$.   Let $f \in \hom(B, A)$. Then we want

$$
\begin{CD}
  \hom(B,B) @>\lambda B>> F(B)\\
  @V\hom(B, f)VV @VVF(f)V\\
  \hom(B, C) @>>\lambda C> F(C)\\
\end{CD}
$$

to commute.

According to MacLane, we are now done; the above diagram fully indicates the proof.  But we still haven't defined what $\lambda$ is given $u$!!  Define $\lambda$ by the following: given $f: B \to C$, take $\lambda_C: \hom(B, C) \to F(B)$ to be the map $g \mapsto F(f)(u)$.  We can see that this is well defined.  Moreover, when $C = B$, we get the desired property $\lambda_B(1_B) = F(1_B)(u) = 1_{F(B)}(u) = u$ since $u \in F(B)$.  I guess the reason this isn't explicitly stated in MacLane is that it's obvious that this is the only choice of $\lambda$ which will make the above diagram commute.



\begin{remark}
  Notice that the Yoneda lemma works with the contravariant hom functor too, ie $\mathbf{Nat}(\hom(-, B), F)$ is isomorphic to $F(B)$ as well. Often it will be stated this way. Proof is analagous, as we just flip all the arrows.
\end{remark}

\begin{remark}
  Implicitly all throughout this discussion we are assuming $\mathcal{C}$ is locally small.  Otherwise what do we mean by $\hom(B,B)$?  We don't really have a notion yet of maps between proper classes.
\end{remark}

\section{Conclusion}

The Yoneda Lemma gives us the following nice functors, called \textbf{Yoneda embeddings}:
\begin{enumerate}
\item The functor $\mathcal{C} \to \mathbf{Set}^{\mathcal{C}}$ defined by $f: A \to B$ maps to the induced natural transformation $\hom(B, -) \to \hom(A, -)$
\item The functor $\mathcal{C} \to \mathbf{Set}^{\mathcal{C}^{\op}}$ defined by $f \mapsto $ the induced natural transformation $$
\hom(-, A) \to \hom(-, B)
  $$
\end{enumerate}

\begin{remark}
  Woah recall how when we were first introducing cohomology we mentioned that the boundary map $\partial: C_{n+1} \to C_{n}$ induced a nice backwards boundary map  $\hom(C_n, G) \to \hom(C_{n +1}, G)$?  Idk if this is related but it kind of looks like it.  I think this is what they were implicitly doing there but they hadn't defined all these other notions yet.
\end{remark}




\end{document}