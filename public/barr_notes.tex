\documentclass{amsart}
\usepackage{amscd}
\theoremstyle{definition}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\theoremstyle{remark}
\newtheorem*{remark}{Remark}
\newtheorem*{answer}{Answer}
\theoremstyle{theorem}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\usepackage{enumitem}
\DeclareMathOperator{\op}{op}
\usepackage{calrsfs}
\newcommand{\q}{q}
\raggedright
\begin{document}

\title{Notes on Barr's Acyclic Models}
\author{Titus Sharman}
\maketitle

\section{Categories}



\begin{definition}
  A \textbf{category} is defined exactly as we know, it's a collection of objects and a collection of morphisms.
\end{definition}

Notice that we can run into set-theoretic issues if the hom-classes are not sets.  To avoid this we refer to the following:

\begin{definition}
  A category where all hom classes are homsets is called \textbf{locally small}
\end{definition}

\begin{example}
Category of sets and maps between sets, category of groups and homomorphisms.  Category of homotopy types; arrows are homotopy classes of continuous maps and objects are topological spaces.
\end{example}

\begin{example}{Important Example}
  We can regard any poset as a category where if $a \leq b$ then there is a morphism from $a$ to $b$
\end{example}

\begin{definition}
  A \textbf{subcategory} is a subset of the category which is closed under composition and stuff.
\end{definition}



\begin{definition}
  

A subcategory $\mathcal{D} \subseteq \mathcal{C}$ is \textbf{full} if $C, D \in \mathcal{D} \implies \hom_{\mathcal{C}}(C, D) = \hom_{\mathcal{D}}(C, D)$
\end{definition}

We can define a \textbf{product} of two categories by taking pairs of objects and pairs of maps.

\begin{definition}
  If $A \in \mathrm{obj}(\mathcal{C})$, the \textbf{slice category} $\mathcal{C}/A$ consists of objects which are morphisms with target $A$, and arrows which are morphisms in $\mathcal{C}$ that make

  $$
  \begin{CD}
    B @>h>> C\\
    @VfVV @VVgV\\
    A @= A\\
  \end{CD}
  $$

  commute
\end{definition}

\begin{remark}
  Why are categories nice?  We are not restricted to considering categories which are sets with additional structure and we can think of a broader class of things.
\end{remark}

\begin{definition}
  An \textbf{isomorphism} is an invertible morphism.
\end{definition}

\begin{definition}
  A \textbf{terminal object} of a category $\mathcal{C}$ is an object $T$ such that $\forall A \in \mathrm{Obj}(\mathcal{C}), \lvert\hom(A, T)\rvert = 1$
\end{definition}

\begin{remark}
  To make talking about dual objects easier, we are going to define an opposite category and then the dual of anything is itself in the opposite category (eg. the initial object is the terminal object in $\mathcal{C}^{\op}$)
\end{remark}

\begin{definition}
  $\mathcal{C}^{\op}$ has the same objects as $\mathcal{C}$, but all the arrows are reversed, e.g. $\hom_{\mathcal{C}}(A, B) = \hom_{\mathcal{C}^{\op}}(B, A)$
\end{definition}

Now, we're going to define category in a new way.  Barr says that this is ``element free'' but it isn't really because we still have a set of arrows and a set of objects.  I guess we'll refine this later.  I'm not going to go through the whole construction here.  They define a category as two sets $A, O$ of arrows and objects, two maps (intuitively associating arrows in $A$ with their domain and codomain) from $A \to O$, a map from $O \to A$, and a map from pairs of composable maps to $A$ which is basically composition.  Then we have four commutative diagrams which describe things like associativity.

Notice that we have to make precise our definition of a function for categories where the arrows are functions.

\begin{definition}
  A \textbf{function} in $\mathbf{Set}$ is an ordered triple $(A, G, B)$ where $A, B \in \mathrm{Obj}(\mathbf{Set})$, $G \subseteq A \times B$, and the composite

  $$
G \hookrightarrow A \times B \rightarrow A
$$
is an isomorphism
\end{definition}

\begin{remark}
Normally we think of a function as associating elements in $A$ with elements in $B$.  This is exactly what's going on here.  $G$ contains a bunch of pairs $(a, b)$ and so it basically encodes the association.  The re\q{}uirement that $G \hookrightarrow A \times B \rightarrow A$ is an isomorphism is just so that each element of $A$ shows up precisely once in the pairs of things in $G$, meaning that  the projection $\pi_1 : G \to A$ is a bijection.
\end{remark}

From this it is easy to see that the empty set is the initial object in \textbf{Set}.

Now we have also an e\q{}uivalent codefinition:

\begin{definition}
  A \textbf{function} is a triple $(A, G^*, B)$ where $G^*$ is the \q{}uotient of the disjoint union $A + B$ by an e\q{}uivalence relation  for which each element of $B$is contained in exactly one e\q{}uivalence class
\end{definition}

\begin{remark}
  What's going on here?  Okay so the disjoint union is like $A \times \{0\} \cup B \times \{1\}$ but we can basically just think of it as the collection of all stuff in $A$ and $B$ except forced to be disjoint.  This e\q{}uivalence class partitions $A + B$ into cosets which contain precisely \textbf{one} element of $B$.  So the cograph is basically elements of $B$ paired with their preimage.  It makes sense why this is the dual to graph, because the graph is basically elements of $A$ paired with their image
\end{remark}

$G$ is called the \textbf{graph} and $G^*$ is called the \textbf{cograph} of the function

\subsection{Exercises}

\subsubsection{}

Show that the following definition of category is e\q{}uivalent to the definition we gave: A \textbf{category} is a set with a partially defined binary operation denoted $\circ$ with the following properties:

\begin{enumerate}[label=(\alph*)]
\item the following statements are e\q uivalent

  \begin{enumerate}[label=(\roman*)]
  \item $f \circ g$ and $g \circ h$ are both defined
  \item $f \circ (g \circ h)$ is defined
  \item $(f \circ g) \circ h$ is defined
  \end{enumerate}
\item If $(f \circ g) \circ h$ is defined, then $(f \circ g) \circ h = f \circ (g \circ h)$
\item for any $f$, there are elements $e,e'$ for whih $e \circ f$ is defined and e\q{}ual to $f$ and $f \circ e'$ is defined and e\q{}ual to $f$
\end{enumerate}

\begin{answer}
We can consider this set as e\q{}uivalent to the set of arrows in our usual definition, and we can consider our binary operation as composition of morphisms.  Then condition (a) is just e\q{}uivalent to closure under composition, condition (b) is e\q{}uivalent to the associativity condition, and condition (c) is e\q{}uivalent to the condition of identity morphisms (ie in our definition $f$ would be a map from $A \to B$ and so $e$ corresponds to $1_A, e'$ corresponds to $1_B$)
\end{answer}

\subsubsection{}

Verify that the following constructions produce categories

\begin{enumerate}[label=(\alph*)]
\item For any category $\mathcal{C}$, the \textbf{arrow category} $\mathrm{Ar}(\mathcal{C})$ of arrows of $\mathcal{C}$ has as objects the arrows of $\mathcal{C}$, and an arrow from $f: A \to B$ to $g: A' \to B'$ is a pair of arrows $h: A \to A'$ and $k: B \to B'$ such that

  $$
  \begin{CD}
    A @>h>> A'\\
    @VfVV @VVgV\\
    B @>>k> B'\\
  \end{CD}
  $$

  commutes

  \begin{answer}

    We need to check a few things.

    First check composition.  Suppose we have two arrows in $\mathrm{Ar}(\mathcal{C})$, such that the first arrow $(h,k)$ is from $f$ to $g$ and the second arrow, $(h',k')$ is from $g$ to $g': A'' \to B''$.  Clearly,

    $$
    \begin{CD}
      A @>h>> A' @>h'>> A''\\
      @VfVV @VgVV @VVg'V\\
      B @>>k> B' @>>k'> B''\\
    \end{CD}
    $$

    commutes, so we are done (ie the composition of these two arrows is just $(h'h, k'k)$

Similarly we can check that closure under composition is inherited from $\mathcal{C}$.  And for a given object $f \in \mathrm{Ar}(\mathcal{C})$, its identity is just $(1_A, 1_B)$
    
  \end{answer}
\item The \textbf{twisted arrow category} of $\mathcal{C}$ is defined the same way as the arrow category except that the direction of $k$ is reversed
  \begin{answer}
    The exact same logic holds as in part (a).  The only difference is we will have $(h,k) \circ (h', k') = (h'h, kk')$ this time.
  \end{answer}
\end{enumerate}

\subsubsection{}

\begin{enumerate}[label=(\alph*)]
\item Show that $h: f \to g$ is an isomorphism in the category of objects of $\mathcal{C}$ over $A$ iff $h$ is an isomorphism of $\mathcal{C}$

  \begin{answer}

    Only if:    Suppose $f: B \to A$ and $h$ is an isomorphism in $\mathcal{C}/A$.  Then

    %SOME SHIT AAAH IDK

 If direction:   Now, suppose $g: C \to A$.  Suppose $h$ is an isomorphism $B \to C$.  Then by definition of slice category $gh = f$.  But this implies $g = fh^{-1}$ by definition of isomorphism. So $h$ is an isomorphism between $g$ and $f$ in $\mathcal{C}/A$

    
    
  \end{answer}

  
\item Give an example of objects $A, B, C $ in a category $\mathcal{C}$ and arrows $f: B \to A, g : C \to A$ such that $B$ and $C$ are isomorphic in $\mathcal{C}$ but $f$ and $g$ are not isomorphic in $\mathcal{C}/A$

  %Pretty nuanced here
\end{enumerate}

\subsubsection{}

Describe the isomorphisms, initial objects, and terminal objects (if they exist) in each of the categories in Exercise 2

\begin{answer}


  
\end{answer}

\subsubsection{}

Describe the initial and terminal objects, if they exist, in a poset regarded as a category.

\begin{answer}
If the poset is totally ordered, and it has a least element, than that's the initial object.  If it is totally ordered and has a greatest element, that's the terminal object.
  
\end{answer}

\subsubsection{}

Show that any two terminal objects in a category are isomorphic by a uni\q{}ue isomorphism.

\begin{answer}
  If $T_1, T_2$ are two terminal objects, then there is a uni\q{}ue map $f$ from $T_1 \to T_2$, and also a uni\q{}ue map $g$ from $T_2$ to $T_1$.  This is the isomorphism because $fg: T_2 \to T_2, gf: T_1 \to T_1$ and because $T_1, T_2$ are terminal objects there is only one map from $T_1$ to itself and from $T_2$ to itself, which is the identity.
  
\end{answer}

\subsubsection{}


\begin{enumerate}[label=(\alph*)]
\item Prove that for any category $\mathcal{C}$ and any arrows $f, g$ of $\mathcal{C}$ such that the target of $g$ is isomorphic to the source of $f$, there is an arrow $f'$ which (i) is isomorphic to $f$ in $\mathrm{Ar}(\mathcal{C})$ and (ii) has source the same as the target of $g$.
\item Use the fact given in (a) to describe a suitable definition of domain, codomain, and composition for a category with one object chosen for each isomorphism class of objects of $\mathcal{C}$ and one arrow from each isomorphism class of objects of $\mathrm{Ar}(\mathcal{C})$.  Such a category is called a \textbf{skeleton} of $\mathcal{C}$
\end{enumerate}

\subsubsection{}

A category is \textbf{connected} if it is possible to go from any object to any other object of the category along a path of composable forward or backward arrows.  Make this definition precise and prove that every category is a union of disjoint connected subcategories in a uni\q{}ue way.

\begin{answer}

  A category is \textbf{connected} if for any $A, B \in \mathrm{Obj}(\mathcal{C})$ there is some collection of objects $A_1, A_2, \ldots, A_n$ such that $\hom(A, A_1) \cup \hom(A_1, A) \not = \emptyset, \hom(A_n, B) \cup \hom(B, A_n) \not = \emptyset$, and for any integer $1 \leq i < n, \hom(A_i, A_{i + 1}) \cup \hom(A_{i + 1}, A_i) \not = \emptyset$

  \bigskip

  Now, given any category $\mathcal{C}$, clearly we can 
\end{answer}

%AAAAH FUCK!!!

\section{Functors}


\begin{definition}
  A \textbf{functor} $F: \mathcal{C} \to \mathcal{D}$ is a map associating objects in $\mathcal{C}$ to objects in $\mathcal{D}$ and arrows in $\mathcal{C}$ to arrows in $\mathcal{D}$ such that the functor respects identity, composition, and source and target.
\end{definition}

\begin{definition}
  A \textbf{contravariant functor} is a map from $\mathcal{C}^{\op} \to \mathcal{D}$
\end{definition}


\begin{definition}
  $F: \mathcal{C} \to \mathcal{D}$ is \textbf{faithful} if it is injective when restricted to each homset, and it is \textbf{full} if it is surjective on each homset.
\end{definition}


\begin{remark}
  ie. we are considering $F|_{\hom(A, B)}: \hom(A, B) \to \hom(F(A), F(B))$ and we want to know whether this map is injective and surjective.
\end{remark}

\begin{definition}
  $F$ \textbf{preserves} a property $P$ that an arrow may have if $f$ having property $P$ implies $F(f)$  has property $P$.  It \textbf{reflects} property $P$ if $f$ has the property whenever $F(f)$ has.
\end{definition}

\begin{example}
  See exercise 1
\end{example}

\begin{example}
For a category whose objects are just sets with additional structure (eg. \textbf{Grp}), the \textbf{underlying set functor} $U: \mathcal{C} \to \mathbf{Set}$ takes each object in $\mathcal{C}$ to its underlying set and maps of course are exactly the same.
\end{example}

\begin{example}
  Given an object $A \in \mathrm{Obj}(\mathcal{C})$ define the \textbf{covariant hom functor}, $\hom(A, -): \mathcal{C} \to \mathbf{Set}$ as follows: For $X \in \mathrm{Obj}(\mathcal{C}), \hom(A, -)(X) = \hom(A, X)$.  For $f: X \to Y \in \mathrm{Ar}(\mathcal{C}), \hom(A, -)(f): \hom(A, X) \to \hom(A, Y)$ is a function given by $g \mapsto f \circ g; \; g \in \hom(A,X)$
\end{example}

There is also a \textbf{contravariant hom functor} $\hom(-, B): \mathcal{C}^{\op} \to \mathbf{Set}$ which is defined analagously.  (Functors from $\mathcal{C}^{\op} \to \mathbf{Set}$ are called \textbf{presheaves} on $\mathcal{C}$ for some reason.)

\bigskip

Notice that functors are maps between categories so we can think of the category of categories, \textbf{Cat}.  I bet this is not locally small because $\mathrm{Obj}(\mathbf{Set})$ is a proper class and so it stands to reason that the functors from $\mathbf{Set} \to \mathbf{Set}$ are also a proper class.  Clearly by definition of isomorphism, two categories $\mathcal{C}, \mathcal{D}$ are \textbf{isomorphic} if there is an invertible functor $F: \mathcal{C} \to \mathcal{D}$.

We have a cooler notion of similarity between categories, however.  This is called \textbf{e\q{}uivalence}.  A functor $F: \mathcal{C} \to \mathcal{D}$ is an \textbf{e\q{}uivalence} if it is full and faithfull and for any $B \in \mathrm{Obj}(\mathcal{D})$, there is an object $A$ of $\mathcal{C}$ such that $F(A)$ is isomorphic to $B$ when considered as objects in $\mathcal{D}$

\subsection{Exercises}


\subsubsection{}

Show that functors preserve isomorphisms, but do not necessarily reflect them.

\begin{answer}

  Suppose $F: \mathcal{C} \to \mathcal{D}$

  Suppose $f: A \to B$ is an isomorphism in $\mathcal{C}$.  Then $F(f) \circ F(f^{-1}) = F(ff^{-1}) = 1_B$.  Similarly $F(f^{-1}) \circ F(f) = 1_A$.  So $F(f)$ is invertible in $\mathcal{D}$ and hence an isomorphism.

Now, consider the functor $F: \mathbf{Grp} \to \mathbf{Grp}$ that sends every group to the trivial group and every group homomorphism to the identity homomorphism on the trivial group.  Clearly, $F(f)$ is an isomorphism for any $f \in \mathrm{Ar}(\mathbf{Grp})$.  But there are lots of group homomorphisms which are not isomorphisms.
  
\end{answer}

\subsubsection{}

Use the concept of arrow category to describe a functor which takes a group homomorphism to its kernel.

\begin{answer}
  


  Consider the arrow category of arrows of \textbf{Grp}.  Then, we can straight up construct a functor $F: \mathrm{Ar}(\mathbf{Grp}) \to \mathbf{Grp}$ that sends every arrow to its kernel.  And suppose we have two objects $f: A \to B, g: A' \to B'$ in the arrow category of \textbf{Grp}.  Then given a morphism $h: A \to A'$, we define $F(h) = h|_{\ker f}$.   We can see that $F(h): F(f) \to F(g)$ and moreover $F_{1_A}  = 1_{F(A)}$.

  Umm this is awkward it seems that it's not going to respect composition tho.

  \end{answer}

\subsubsection{}


Show that the following define functors:

\begin{enumerate}[label=(\alph*)]
\item The projection map from a product $\mathcal{C} \times \mathcal{D}$ of categories to one of them

  \begin{answer}
    Since maps in the product category are defined componentwise, it is clear that the projection map will respect source and target, composition, and identity.
  \end{answer}
\item for $\mathcal{C}$ a category and an object $A$ of $\mathcal{C}$, the constant map $F$ from a category $\mathcal{B} \to \mathcal{C}$ which takes every object to $A$ and every arrow to $1_A$

  \begin{answer}
    Suppose $f: A' \to B$ is a morphism in $\mathcal{B}$.  Clearly $F$ respects source and target because the source and the target of $F(f)$ is just $A$ for any $f$.  Also $F$ respects identity, because $F(f) = 1_A$ for any $f$.  And $F$ respects composition because $1_A \circ 1_A = 1_A$
  \end{answer}

  \begin{remark}
    We actually assumed that this was a valid functor in the solution to exercise 1.
  \end{remark}
\item The forgetful functor from the category $\mathcal{C}/A$ of object over $A$ to $\mathcal{C}$ which takes on object $B \to A$ to $B$ and an arrow $h: B \to C$ over $A$ to itself.

  \begin{answer}
Suppose $f: B \to A, g: C \to A$ are two objects in the slice category.  Then $F(h): B \to C$ which is fine because $F(f) = B, F(g) = C$. In likewise manner $F$ respects composition.  And $1_B \mapsto 1_B$ so $F$ respects identity morphisms as well.
  \end{answer}
\end{enumerate}

\section{Natural Transformations}

\begin{definition}
  If $F: \mathcal{C} \to \mathcal{D}, G: \mathcal{C} \to \mathcal{D}$ are two functors, a \textbf{natural transformation} $\lambda: F \to G$ associates each object $C \in \mathrm{Obj}(\mathcal{C})$ with arrow $\lambda C$ such that

  $$
  \begin{CD}
    F(C) @>\lambda C>> G(C)\\
    @VF(g)VV @VVG(g)V\\
    F(C') @>>\lambda C'> G(C')\\
  \end{CD}
  $$

  commutes.
\end{definition}

\begin{definition}
  The arrows $\lambda C$ are the components of $\lambda$
\end{definition}

\begin{definition}
  $\lambda$ is a \textbf{natural e\q{}uivalence} if each component of $\lambda$ is an isomorphism in $\mathcal{D}$
\end{definition}

\begin{example}{Quintessential Example, Apparently}
  The natural map from a finite dimensional vector space $V$ to $V^{**}$ is a natural e\q{}uivalence.
\end{example}
\begin{example}
  The Hurewicz transformation from the fundamental group of a topological space to its first homology group is also a natural transformation of functors
\end{example}
\begin{remark}
  woah this is cool no idea there was a transformation between fundamental group and first homology group
\end{remark}

\begin{example}
  Fix categories $\mathcal{C}, \mathcal{D},$ with $\mathcal{C}$ small.  Then the collection of functors from $\mathcal{C} \to \mathcal{D}$ is a category with natural transformations as arrows.  This is called a \textbf{functor category} and is often denoted $\mathcal{D}^{\mathcal{C}}$
\end{example}

\begin{remark}
  Why the condition that $\mathcal{C}$ has to be small?  They seem to do this so that for two functors $F, G: \mathcal{C} \to \mathcal{D}$, there will be no more than a set of natural transformations $F \to G$.  But seems to me \textit{a priori} there's no reason why $\mathcal{D}^{\mathcal{C}}$ has to be locally small.
\end{remark}

\subsection{Exercises}

\subsubsection{}

Show how to describe a natural transformation as a functor from an arrow category to a functor category.

\begin{answer}

  
\end{answer}

\end{document}