\documentclass[oneside,11pt]{amsbook}

\usepackage{styling}
\usepackage{lipsum}
\begin{document}

\title{Notes on Topology\\
\large with Worked Examples\\ and Pictures
}



\author{Titus Sharman}

\maketitle

\tableofcontents

\chapter*{Preface}


These topology notes represent my first attempt to write some long-form expository notes.  Main sources consulted include \cite{Lawson} for general topology, \cite{Vick} for homology, \cite{Hatcher} for general algebraic topology, \cite{Hatcher2} (hereafter ``mini Hatcher'') for introductory point-set topology, and \cite{Bradley} for insights into the categorical viewpoint.  Any examples or arguments drawn from these books are of course specifically cited, except for things like the topologist's sine curve which show up so many places I don't know where they originated.

These notes are arranged by first introducing basic point-set topology definitions, then moving on to homology, with the motivating example of De Rahm cohomology, then finally homotopy and the associated machinery for computing it (covering spaces, Van Kampen, etc).  At the end are two appendices containing the prerequisite material.  
% , as well as from topology class in Autumn 2020, taught by Steve Kerckhoff and Francisco Herrera, analysis class in Winter 2021, taught by Andras V{\'a}sy and Lie Qian, and the DRP in Fall 2020, taught by Shintaro Fushida-Hardy.

%The whole text is inspired by these sources, but whenever a particular exercise, definition, or example is incorporated here, it'll be explicitly cited.  (Except examples which show up in a whole bunch of literature, like the topologist's sine curve).

%For formatting, the choice of font and use of side notes is inspired by the Tufte\TeX\ package, which is in turn inspired by Edward Tufte's work on visualization.  All errors are my own.



\chapter{Topological Spaces}

\section{What is a topological space?}



Topology is the study of \emph{topological spaces}.  Lots of times we'll see topological spaces with additional structure pop up\mymarginpar{e.g. Hilbert Spaces, metric spaces, manifolds, normed linear spaces, etc.}, and topology is underlying the study of all those things.

We first want to define topological spaces so we can discover their properties.  Topological spaces are the most general \emph{category} in which the maps are \emph{continuous}.  So to define topological spaces, we first need to define \emph{continuous functions}.

\begin{definition}\label{continuous}

A \emph{continuous function} is a function where the preimage of every \emph{open} set is \emph{open}.
  
\end{definition}
\begin{figure}[h]
  \caption{A continuous function}
\incfig{fig1}
  \label{fig:continuous}
\end{figure}
\begin{figure}[h]
  \caption{A function which is not continuous}
\incfig{fig2}
  \label{fig:discontinuous}
\end{figure}

Unfortunately we haven't yet defined what an open set means in full generality, but we do at least know what this means in the case of \rr.  An open set in \rr\ is just the union of open intervals.\mymarginpar{The empty union counts!  Otherwise the preimage of any set not contained in the range wouldn't be open.}

How do we make sense of this definition of continuous function?  A first attempt might be to compare this to the definition we know and love from calculus:

\begin{definition}\label{rcontinuous}
  A \emph{continuous function}  $f: \rr \to \rr$ is a function such that for every $\epsilon > 0$ and every point $x \in \rr$, there exists $\delta > 0$ such that $f((x - \delta, x + \delta)) \subseteq (f(x) - \epsilon, f(x) + \epsilon)$.
\end{definition}

\begin{remark}

  These two definitions of continuity are the same for maps from $\rr \to \rr$.


\end{remark}

\begin{proof}
  First, \ref{rcontinuous} implies \ref{continuous}.  Suppose $f: \rr \to \rr$ is continuous under definition \ref{rcontinuous}.  Let $A \subseteq \rr$ be open.  Then consider any $x \in f^{-1}(A)$.  Since $A$ is the union of open intervals, $f(x) \in (c, d) \subseteq A$ for some $c, d \in \rr$.  Taking $\epsilon = \min\{\lvert f(x) - c \rvert, \lvert f(x) - d\rvert\}$, we have that $(f(x) - \epsilon, f(x) + \epsilon) \subseteq A$.  But by definition \ref{rcontinuous}, this implies that there exists a $\delta$ such that $(f(x) - \epsilon, f(x) + \epsilon) \supseteq f^{-1}((x - \delta, x + \delta))$. Since $(f(x) - \epsilon, f(x) + \epsilon) \subseteq A$, this in turn implies that $(x - \delta, x + \delta) \subseteq f^{-1}(A)$.

  Repeating this argument for every point gives a collection of intervals $(x_i - \delta_i, x_i + \delta_i)$.  Now, notice that $f^{-1}(A)$ is contained in the union of this collection since every point $x_i \in f^{-1}(A)$ is certainly in its corresponding interval.  But also, the union of this collection is contained in $f^{-1}(A)$ since every one of the individual intervals is, by the previous paragraph.  Thus, $f^{-1}(A)$ is the union of open intervals, so it's open.

  % Since open sets in \rr\ are unions of open intervals, suffices to show that the preimage of any open interval in $\rr$ is open.
  % \begin{scratch}
  %   repeat for all $x \in f^{-1}(A)$ lol and then take union over the little balls around each $x$
  % \end{scratch}

  \medskip

  Now, let's do the direction \ref{continuous} implies \ref{rcontinuous}.  Let $x \in \rr, \epsilon > 0$.  Then by definition \ref{continuous}, the preimage of $(f(x) - \epsilon, f(x) + \epsilon)$ is open.  Since it's open, it's the union of open intervals.  So there's some $c,d \in \rr$ such that $x \in (c,d)$ and $(c,d) \subseteq f^{-1}((f(x) - \epsilon, f(x) + \epsilon))$.  Taking $\delta = \min\{\lvert x - c\rvert, \lvert x - d\rvert\}$ gives $(x - \delta, x + \delta) \subseteq f^{-1}((f(x) - \epsilon, f(x) + \epsilon))$, as desired.
\end{proof}


% \begin{scratch}
%   Maybe we want to add some more motivation here at some point for definition \ref{continuous}
% \end{scratch}

The great thing about definition \ref{continuous} is that it doesn't rely on the highly \rr-specific definition of open sets in terms of open intervals, and just uses open sets in some general way.  We hope to abstract away the specificity here and cut to the core of what makes open sets nice in \rr.
\begin{figure}[h]
  \caption{The idea of a topological space}
\incfig{fig3}
\end{figure}
\begin{definition}\label{topospace}
  A \emph{topological space} consists of the following data:

  \begin{itemize}
  \item A set $X$
  \item A subset $\mathcal{T} \subseteq 2^X$ which
    \begin{enumerate}
    \item Is closed under union
    \item Is closed under finite intersection
    \item Contains $\emptyset$ and $X$.\mymarginpar{Notice that $\mathcal{T}$ isn't re\q{}uired to be closed under complement. We'll call the complement of an open set a \emph{closed set}.}
    \end{enumerate}
  \end{itemize}
\end{definition}

\begin{definition}\label{opensets}
  The elements $S \in \mathcal{T}$ are called \emph{open sets}.
\end{definition}

\begin{definition}
  If $x \in X$, then an open set containing $x$ is called a \emph{neighborhood} of $x$.
\end{definition}



\begin{caution}
Despite our intuition from English, \emph{open} $\nLeftrightarrow$ \emph{closed}.  For example, notice that since $\emptyset = X^C$, it is the complement of an open set and is therefore closed.  But by definition $\emptyset$ is open.  So $\emptyset$ is both open and closed.
\end{caution}

\begin{remark}

  Topological spaces and continuous maps form a category, hereafter denoted as \textbf{Top}. The identity will be the identity map as sets, associativity is clear, and composition is well defined because $f: X \to Y$ and $g: Y \to Z$ are continuous maps, then for any  open set $z \subseteq Z$, $(gf)^{-1}(z) = f^{-1}(g^{-1}(z)) = f^{-1}(\text{an open set})$ which is open.

\end{remark}

\begin{definition}

Isomorphisms in \textbf{Top} are called \emph{homeomorphisms}.
  
\end{definition}




\begin{example}
  $(\rr, \mathcal{T})$ where every set in $\mathcal{T}$ is a union of intervals of the form $(a, b)$, where $a, b \in \rr$, is a topological space.  We'll hereafter refer to this as simply \rr\ with the standard topology.
\end{example}

Why does $\mathcal{T}$ have to satisfy these 3 properties?  Let's do a sanity check to see if they are satisfied by the above example.

\begin{enumerate}
\item Suppose $S_i: i \in I$ is a collection of open sets in \rr.  Then $S_i = \cup_{j \in I_i} (a_{i_j}, b_{i_j})$ for some index set $I_i$.  Then, we have that

  \[
\cup_{i \in I} S_i = \cup_{i \in I}\left(\cup_{j \in I_i} (a_{i_j}, b_{i_j})\right) = \cup_{k \in \cup_{i}I_i} (a_k, b_k)
\]

So the union of the $S_i$ still the union of some open intervals, and it's still an open set.
\item Suffice to show that the pairwise intersection of open sets in \rr\ is open, cause then we can just do induction.  Let $S_1 = \cup_{i \in I_1} (a_i, b_i)$ and $S_2 = \cup_{j \in I_2}(a_j, b_j)$.  Then, we have

\begin{multline*}
\indent\indent S_1 \cap S_2 = \left( \cup_{i \in I_1} (a_i, b_i)\right) \cap \left(\cup_{j \in I_2}(a_j, b_j)\right)\\ = \cup_{(i,j) \in I_1 \times I_2}\left((a_i, b_i) \cap (a_j, b_j)\right)\\
\end{multline*}

by De Morgan. But this open since any pairwise intersection of open intervals is nothing but another open interval.
\item $\emptyset$ is the empty union and $\rr = \cup_{i \in \zz}(-i, i)$
\end{enumerate}

We've defined the standard topology on \rr\ by open sets being unions of open intervals.  The reason for this is to motivate property (1).  See \cite{Chen} or \cite{Lawson} for a more standard metric-space centric approach.  However, our example does naturally lead to the following:

\begin{definition}
  Let $(X, \mathcal{T})$ be a topological space.  A \emph{basis} $\mathcal{B}$ for the topology $\mathcal{T}$ is a subset of $\mathcal{T}$ such that every element of $\mathcal{T}$ is the union of some elements of $\mathcal{B}$.
\end{definition}

\begin{example}
  Open intervals form a basis for the standard topology on \rr.
\end{example}

\begin{definition}
  Let $(X, \mathcal{T})$ be a topological space. \mymarginpar{You may be wondering: what's a \emph{first countable} space? This is a space which has a countable local basis at every point.  It's a weaker condition than second countability and we won't need it now.} $(X, \mathcal{T})$ is called \emph{second countable} if it has a countable basis.
\end{definition}

What about property (2)?  It seems that arbitrary intersections should be open.  To see why this doesn't correspond with our motivating example, consider in \rr\ the collection of sets $A_i := \left(-\frac{1}{i}, \frac{1}{i}\right)$.  Then $\cap_{i \in \zz^+} A_i = \{0\}$ which is not open.

As for property (3), this was already remarked on in a margin note.  Basically we want to still have continuous functions which are constant or where the range is a proper subset of the codomain.



\section{Examples of Topological Spaces}

So far, we've defined a topological space, motivating everything purely by the sole example of \rr.  To get a notion of why this abstraction is useful, it helps to have some other examples, which we'll be able to appeal to when we're trying to visualize topological spaces.  We'll leave it to the reader to check that properties (1)--(3) hold for most of these.  For more, see the {\color{teal}\href{https://en.wikipedia.org/wiki/List_of_topologies}{list of topologies}} on wikipedia.

\begin{example}[Discrete Topology]

  Given any set $X$, let $\mathcal{T} = 2^X$.  Clearly this has more open sets than any other topology on $X$, in the sense that if $\mathcal{S}$ is another topology on $X$, then $\mathcal{S} \subseteq \mathcal{T}$.

  Similarly, since there are more open sets in $(X, \mathcal{T})$ than in $(X, \mathcal{S})$, there will be more continuous functions out of $(X, \mathcal{T})$, and fewer continuous functions into $(X, \mathcal{T})$.  These notions will be made precise later on.
  
\end{example}

\begin{example}[Indiscrete/Trivial Topology]

  Given any set $X$, let $\mathcal{T} = \{\emptyset, X\}$.  No topology on $X$ can have fewer open sets, cause property (3) forces us to have at least the whole space and the empty set be open.  Conversely to the previous example, the indiscrete topology has fewer open sets than any other topology.

  % Also conversely to the discrete topology, there are fewer continuous functions out of $X$ under the discrete topology than under any other topology.  
  
 \end{example}

 To get a feel for the difference between the discrete and indiscrete topology, let's think about what happens when we apply them to \rr.  Let $\mathcal{I}$ be the indiscrete topology on \rr, $\mathcal{D}$ be the discrete topology on \rr, and $\mathcal{S}$ be the standard topology on \rr.  Now, consider the identity function $f: \rr \to \rr$ defined by $x \mapsto x$.  Is $f$ continuous?  We know from calculus that $f$ is continuous from $(\rr, \mathcal{S}) \to (\rr, \mathcal{S})$.  But how about with respect to these other topologies?  

 \begin{description}[format=\normalfont]
 \item[Indiscrete to standard] In this case, $f$  \textbf{is not} continuous.  For example, $f^{-1}((0,1)) = (0,1) \not \in \mathcal{I}$.
 \item[Discrete to standard]  In this case, $f$ \textbf{is} continuous.  Indeed, since $\mathcal{S} \subseteq \mathcal{D}$, and the preimage of any set under the identity function is simply that set itself, the preimage of any set in $\mathcal{S}$ will be in $\mathcal{D}$
 \item[Standard to indiscrete] In this case, $f$ \textbf{is} continuous.  Indeed, $f^{-1}(\rr) = \rr \in \mathcal{S}$ and $f^{-1}(\emptyset) = \emptyset \in \mathcal{S}$
 \item[Standard to discrete] In this case, $f$ \textbf{is not} continuous.  For example, $f^{-1}(\{0\}) = \{0\} \not \in \mathcal{S}$
 \end{description}

 These cases illustrate the following general principle: topological spaces with lots of open sets have more continuous functions mapping out and fewer continuous functions mapping in than topological spaces with few open sets.  We want a word to describe this phenomenon.

 \begin{definition}

Let $X$ be a set, and let $\mathcal{T}, \mathcal{S}$ be topologies on $X$.  If $\mathcal{S} \subseteq \mathcal{T}$, then we say $\mathcal{S}$ is \emph{coarser} or \emph{weaker} than $\mathcal{T}$.  We say that $\mathcal{T}$ is \emph{finer} or \emph{stronger} than $\mathcal{S}$
   
\end{definition}

The choice of the word weak is deliberate: a stronger topological space has more continuous functions going out of it, and a weaker one has fewer continuous functions going out of it.  In the most extreme case, observe that every function out of a discrete space is continuous, since the preimage of anything is still a subset of the domain.


 \begin{figure}[h]
   \caption{The red topology is finer than the green topology.  Every set which is open in the green topology is open in the red topology, but there are some sets which are open in red but not in green.}
   \label{fig:finer}
   \incfig{fig5}
 \end{figure}

 \begin{definition}
  Given a set $X$ and a family of functions $\mathcal{H}$ from $X$, the \emph{initial topology} on $X$ is the weakest topology which makes every function in $\mathcal{H}$ continuous.\mymarginpar{This is also called the \emph{$\mathcal{H}$-weak topology} in \cite{ReedandSimon}}
\end{definition}

\begin{example}
Consider the identity function $f: \rr \to \rr$, where the codomain is given the standard topology.  Let $\mathcal{H} = \{f\}$.  Then, the initial topology on \rr\ is also the standard topology, since the preimage of every set is simply itself, so nothing coarser than the topology on the codomain will suffice.
\end{example}

\begin{definition}

  Given a set $Y$ and a family of functions $\mathcal{H}$ with codomain  $Y$, the \emph{final topology} on $Y$ is the strongest topology which makes every function in $\mathcal{H}$ continuous.
  
\end{definition}

Notice that we haven't really discussed topologies on finite sets.  Let's introduce two examples.

\begin{example}[Sierpinski Space]
  Let $X = \{a, b\}$, $\mathcal{T} = \{\emptyset, \{a\}, X\}$.  This is the smallest topological space which is neither discrete nor indiscrete.
\end{example}

\begin{example}[Pseudocircle]
  Let $X = \{N, S, E, W\}, \mathcal{T} = \{\emptyset, \{N\}, \{S\}, \{N, W, S\}, \{N, E, S\}, X\}$.

  \mymarginpar{This example originally explained to me by SFH.
    Originally introduced in full generality by
    \cite{McCord}}

At first glance, the pseudircle seems like a random four point space, but the motivation for it is that it's designed to mimic a circle.  Indeed, you can think of it as a circle with the open top semicircle identified as a single point, the open bottom semicircle identified as a single point, and the two points left over left unchanged. It turns out that the pseudocircle is homology e\q{}uivalent to $\mathbb{S}^1$, an idea which we'll explore later on.


\begin{figure}
  \label{fig:pseudocircle}
    \caption{The pseudocircle}
    \incfig{fig4}
    \end{figure}
  \end{example}

  

\begin{example}[Product Topology]
  % Topological spaces and continuous maps form a category.  This means that we ought to be able to find some notion of a product.  That is, given two topological spaces $(X, \mathcal{X})$ and $(Y, \mathcal{Y})$ we'd expect there to be some space $X \times Y$ such that maps into $X$ and $Y$ factor uni\q{}uely through $X \times Y$.  A reasonable guess is that the set $X \times Y$ would be the cartesian product of $X$ and $Y$ as sets.  What about the topology on $X \times Y$?  Naively, we might guess that the topology on $X \times Y$ would have a basis given by cartesian products of open sets in $\mathcal{X}$ and open sets in $\mathcal{Y}$.  This is called the \emph{box topology}.

Given two topological spaces, $X$ and $Y$,   the \emph{product topology} or \emph{Tychonoff topology} on $X \times Y$ is the initial topology for the projections $\pi_X, \pi_Y$.
  \[
  \begin{tikzcd}
    & A \arrow[ld] \arrow[d] \arrow[rd] & \\
    X  & X \times Y \arrow[l, "\pi_X"] \ar[r, "\pi_Y"'] & Y \\
  \end{tikzcd}
\]
  \begin{scratch}
    Insert commutative diagram for product here.  Also, explain why the box topology doesn't work.  Maybe refer to Bradley.  In both these examples clarify the meaning of the commutative diagram.
  \end{scratch}

  
  
\end{example}

\begin{example}[Subspace Topology]

  Given a topological space $(X, \mathcal{T})$ and a subset $Y \subseteq X$, the \emph{subspace topology} on $Y$ is the initial topology for the inclusion $\iota: Y \to X$. \mymarginpar{Here, I'm following the universal property as characterized in \cite{Bradley}}
 E\q{}uivalently, for any topological space $Z$, we want the following diagram to commute
\[
  \begin{tikzcd}
    Z \ar[d] \ar[dr] & \\
    Y \ar[r, "\iota"] & X\\
  \end{tikzcd}
\]
\end{example}

\begin{example}[Manifolds]

  So far, we haven't yet discussed the classic pictures you see on the covers of topology books: tori, spheres, knots, M{\"o}bius strips.  All of these shapes are called \emph{manifolds}\footnote{Technically a M{\"o}buis strip is a manifold with boundary\ldots}, and they are all characterized by looking locally like $\rr^n$ for some $n$.  This is why, for instance, one might think that the Earth is flat, since locally it looks like a plane.

  \begin{definition}
    A \emph{manifold} $(M, \mathcal{T})$ is a second countable, Hausdorff, topological space which is locally homeomorphic to $\rr^n$ for some $n$.  To state locally homeomorphic more precisely, I mean that for any point $x \in M$, there exists some neighborhood $U \ni x$ such that $U$ is homeomorphic to an open set in $\rr^n$.  $n$ is called the \emph{dimension} of the manifold.
  \end{definition}
  % Whitney embedding theorem

The second countable condition mainly exists to avoid some pathological counterexamples.  The main thing to think about is that an $n$-dimensional manifold locally behaves like $\rr^n$ in a topological sense.  
\end{example}

\begin{scratch}
  Insert some pictures of manifolds here.  ALso reference counterexample for second countability.  Insert a zoomed in picture of a piece of string in a knot, reference locally homeomorphic to $\rr^n$.
\end{scratch}

\begin{joke}
  A donut is homeomorphic to a coffee cup!  To a topologist, they are indistinguishable.
\end{joke}

What about the Hausdorff condition?  It means that distinct points can be separated by open sets.  Why is this important, and why is it so key in characterizing $\rr^n$?  We attempt to answer these \q{}uestions and related \q{}uestions in the next section.


\section{Separation and Connectivity}


The separation axioms for a topological space $(X, \mathcal{T})$ are as follows:

\begin{enumerate}[label=(T\arabic*)]
\item For distinct $x, y \in X$, there exists open sets $U_x, U_y$ such that $U_x \ni x$ but $U_x \not \ni y$ and $U_y \ni y$ but $U_y \not \ni x$
\item For distinct $x, y \in X$, there exist open sets $U_x, Y_y$ such that $U_x \ni x, U_y \ni y$, and $U_x \cap U_y = \emptyset$
\item For $x \in X$, and $Y$ a closed subset of $X$, such that $x \not \in Y$, there exist open sets $U_x, U_y$ such that $U_x \ni x, U_y \supseteq Y$, and $U_x \cap U_y = \emptyset$
\item For $Y, Z$ closed disjoint subsets of $X$, there exist open sets $U_y, U_z$ such that $U_y \supseteq Y, Y_z \supseteq Z$, and $U_y \cap U_z = \emptyset$
\end{enumerate}

\begin{figure}[h]
  \caption{The Separation Axioms}
  \incfig{fig6}
  \label{fig:separation}
\end{figure}

% \begin{scratch}
%   Can someone verify my DeMorgan computation above?
% \end{scratch}



\section{The Quotient Topology}






\chapter{Homology}

The goal of this chapter is to create a functor from $\mathbf{Top} \to \mathbf{Grp}$ which we'll use to distinguish between topological spaces.  

\section{Simplicial Homology}

Simplicial homology is the homology of \emph{simplicial complexes}, which are a special type of topological space.  A simplex is basically a triangle, generalized to higher dimensions.

\begin{figure}[h]
  \caption{A triangle}
  \incfig{fig7}
\end{figure}


\begin{definition}
  A set $S \subseteq \rr^n$ is \emph{convex} if the line segment connecting any two points $x, y \in S$ is itself contained in $S$.  The \emph{convex hull} of a set of points in $\rr^n$ is the smallest convex set containing all those points.
\end{definition}

\begin{definition}

An $n$-simplex is the convex hull of $n  +1 $ affinely independent points.\mymarginpar{\emph{affinely independent} means that if take the set of lines connecting these points and translate these lines so that they pass through the origin, they become linearly independent.  Intuitively, we're forgetting the origin.}
  
\end{definition}



\section{Singular Homology}



\section{CW Homology}

\chapter{Forms and De Rahm Cohomology (todo)}

\chapter{Homotopy (todo)}

\section{The Fundamental Group: The First Homotopy Group}


\chapter*{Appendix on Category Theory}

\chapter*{Appendix on Multilinear Algebra}



\begin{bibdiv}
  \begin{biblist}
    
\bib{your-cite-key}{webpage}{
    author={tentaclenorm},
    title={Is it possible to cite a webpage using amsrefs?},
    subtitle={},
    date={2012-08-05},
    url={https://tex.stackexchange.com/q/65972/},
    accessdate={2021-06-11},
    note={},
}
        \bib{Barr}{book}{
      title={Acyclic Models},
      author={Barr, Michael},
      date={2002},
      publisher={American Mathematical Society}
    }
    \bib{Bradley}{book}{
      title={Topology: A Categorical Approach},
      author={Bradley, Tai-Danae},
      author={Bryson, Tyler},
      author={Terilla, John},
      date={2020},
      publisher={MIT Press}
    }
        \bib{Cartan}{book}{
      title={Homological Algebra},
      author={Cartan, Henri},
      author={Eilenberg, Samuel},
      date={1956},
      publisher={Princeton University Press},
      address={Princeton, New Jersey}
    }
    \bib{Chen}{book}{
      title={An Infinitely Large Napkin},
      author={Chen, Evan},
      date={2021}
      }
    \bib{Hatcher}{book}{
      title={Algebraic Topology},
      author={Hatcher, Allen},
      date={2002},
      publisher={Cambridge University Press},
      address={New York}
    }
    \bib{Hatcher2}{book}{
      title={Notes on Introductory Point-Set Topology},
      author={Hatcher, Allen},
      date={2005}
      }
    \bib{Lang}{book}{
      title={Algebra},
      author={Lang, Serge},
      date={2002},
      publisher={Springer},
      address={New York}
    }
    \bib{Lawson}{book}{
      title={Topology: A Geometric Approach},
      author={Lawson, Terry},
      date={2003},
      publisher={Oxford University Press},
      address={New York}
    }
    \bib{McCord}{article}{
      title={Singular homology groups and homotopy groups of finite topological spaces},
      author={McCord, Michael},
      journal={Duke Math Journal},
      volume={33},
      date={September 1966},
      pages={465--474}
      }
    \bib{ReedandSimon}{book}{
      title={Functional Analysis},
      author={Reed, Michael},
      author={Simon, Barry},
      date={1981},
      publisher={Elsevier Science}
      }
    \bib{Vick}{book}{
      title={Homology Theory: An Introduction to Algebraic Topology},
      author={Vick,James},
      date={1994},
      publisher={Springer},
      address={New York}
      }
  \end{biblist}
\end{bibdiv}




\end{document}